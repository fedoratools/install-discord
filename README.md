## Instalare Discord pentru Fedora

Acest script bash simplifică procesul de instalare a Discord pe sistemele Fedora. Este util pentru utilizatorii care doresc să instaleze rapid și ușor Discord-ul pe distribuțiile lor Fedora.

### Cum se folosește

1. Descărcați scriptul sau copiați-l în sistemul dvs. Fedora.
2. Deschideți un terminal și navigați la directorul în care ați salvat scriptul.
3. Acordați permisiunile de execuție pentru script folosind comanda:

```bash
chmod +x install_discord.sh
```

4. Rulați scriptul folosind comanda:

```bash
./install_discord.sh
```

### Ce face acest script

- Adaugă depozitul nonfree RPM Fusion pentru a permite instalarea pachetelor nonfree.
- Actualizează lista de depozite pentru a asigura că sunt disponibile cele mai recente pachete.
- Instalează Discord folosind gestionarul de pachete DNF.

**Notă:** Acest script necesită privilegii de sudo pentru a putea instala pachete și actualiza lista de depozite.