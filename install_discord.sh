#!/bin/bash

# Adaugă depozitul nonfree RPM Fusion
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm

# Actualizează lista de depozite
sudo dnf update

# Instalează Discord
sudo dnf install discord

